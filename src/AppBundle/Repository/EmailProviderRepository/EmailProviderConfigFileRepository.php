<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 22:54
 */

namespace AppBundle\Repository\EmailProviderRepository;


use AppBundle\Exception\MissingEmailProviderFieldsException;
use AppBundle\Exception\WrongEmailProviderException;
use AppBundle\Interfaces\Repository\EmailProviderRepository\EmailProviderRepositoryInterface;
use AppBundle\ValueObject\Email\EmailConfig;

/**
 * Why Repository? Well, maybe in a future we can get email provider information through a database,
 * instead of config files. If this happens my only task will add a new repository that gets the information
 * through the database, the code won't break and we have changed the data source with minimal effort!
 */
class EmailProviderConfigFileRepository implements EmailProviderRepositoryInterface
{
    private $availableProviders;
    private $providerMailConfig;

    /**
     * EmailProviderConfigFileRepository constructor.
     * @param $availableProviders
     * @param $providerMailConfig
     */
    public function __construct($availableProviders, $providerMailConfig)
    {
        $this->availableProviders = $availableProviders;
        $this->providerMailConfig = $providerMailConfig;
    }

    public function getAll(): array
    {
        $providers = [];

        foreach ($this->providerMailConfig as $providerName => $providerConfig) {
            $this->checkDataIntegrity($providerConfig);
            $providers[] = new EmailConfig($providerName, $providerConfig);
        }

        return $providers;
    }

    public function getByName(string $name): EmailConfig
    {
        if (!in_array($name, $this->availableProviders)) {
            throw new WrongEmailProviderException();
        }

        $emailConfigData = $this->providerMailConfig[$name];
        $this->checkDataIntegrity($emailConfigData);

        return new EmailConfig($name, $emailConfigData);
    }

    private function checkDataIntegrity(array $data)
    {
        if (!isset($data['server_name']) ||
            !isset($data['username'])    ||
            !isset($data['password'])    ||
            !isset($data['port'])) {

            throw new MissingEmailProviderFieldsException();
        }
    }

}