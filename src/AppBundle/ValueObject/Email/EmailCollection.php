<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 23:19
 */

namespace AppBundle\ValueObject\Email;


class EmailCollection extends \ArrayObject
{
    /**
     * @param Email $email
     *
     * I can't edit the type of the param because this method is inherit from ArrayObject, so i do type checking inside method,
     * i can write my own implementation of an Collection, but i think it's to overhead for small exercise.
     */
    public function append($email)
    {
        if ($email instanceof Email) {
            parent::append($email);
        }
    }

}