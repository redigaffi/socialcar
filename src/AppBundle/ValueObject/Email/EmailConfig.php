<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 22:41
 */

namespace AppBundle\ValueObject\Email;


class EmailConfig
{
    private $providerName;
    private $serverName;
    private $username;
    private $password;
    private $port;

    /**
     * EmailConfig constructor.
     * @param string $name
     * @param array $config
     */
    public function __construct(string $name, array $config)
    {
        $this->providerName = $name;
        $this->serverName = $config['server_name'];
        $this->username = $config['username'];
        $this->password = $config['password'];
        $this->port = $config['port'];
    }

    /**
     * @return string
     */
    public function getProviderName(): string
    {
        return $this->providerName;
    }


    /**
     * @return mixed
     */
    public function getServerName(): string
    {
        return $this->serverName;
    }

    /**
     * @return mixed
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getPort(): string
    {
        return $this->port;
    }



}