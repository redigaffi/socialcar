<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 22:47
 */

namespace AppBundle\ValueObject\Email;


class Email
{
    private $subject;
    private $sender;
    private $recipient;
    private $attachments;
    private $body;

    /**
     * Email constructor.
     * @param $subject
     * @param $sender
     * @param $recipient
     * @param $body
     */
    public function __construct(string $subject, string $sender, string $recipient, string $body)
    {
        $this->subject = $subject;
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->body = $body;
    }

    /**
     * @param array $attachments
     */
    public function setAttachments(array $attachments)
    {
        $this->attachments = $attachments;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getSender(): string
    {
        return $this->sender;
    }

    /**
     * @return string
     */
    public function getRecipient(): string
    {
        return $this->recipient;
    }

    /**
     * @return string
     */
    public function getAttachments(): string
    {
        return $this->attachments;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

}