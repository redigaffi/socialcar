<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 19/09/17
 * Time: 0:13
 */

namespace AppBundle\Service\Email;


use AppBundle\Interfaces\Repository\EmailProviderRepository\EmailProviderRepositoryInterface;
use AppBundle\Interfaces\Service\Email\EmailSenderInterface;
use AppBundle\ValueObject\Email\Email;
use AppBundle\ValueObject\Email\EmailCollection;
use AppBundle\ValueObject\Email\EmailConfig;

/**
 * Like the exercise asked for an algorithm that sends the emails to random providers to balance the traffic.
 *
 * In this class we could implement different algorithm's for the load balancing like:
 *  - Round Robin (sometimes called "Next in Loop").
 *  - Weighted Round Robin
 *  - Random (Implemented)
 *  - Least latency
 *  - ...
 *
 */
class EmailSenderLoadBalancedService
{
    private $emailSender;
    private $emailProviderRepository;

    /**
     * EmailSenderLoadBalancedService constructor.
     * @param $emailSender
     * @param $emailProviderRepository
     */
    public function __construct(EmailSenderInterface $emailSender, EmailProviderRepositoryInterface $emailProviderRepository)
    {
        $this->emailSender = $emailSender;
        $this->emailProviderRepository = $emailProviderRepository;
    }

    public function sendEmailsToRandomProviders(EmailCollection $collection)
    {
        $availableProviders = $this->emailProviderRepository->getAll();
        $amountOfProviders = count($availableProviders)-1;

        /**
         * @var Email $email
         */
        foreach ($collection as $email) {
            $randomProvider = $availableProviders[random_int(0, $amountOfProviders)];
            $this->emailSender->sendEmail($randomProvider, $email);
        }
    }


}