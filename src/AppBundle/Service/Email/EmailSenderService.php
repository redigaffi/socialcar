<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 22:44
 */

namespace AppBundle\Service\Email;


use AppBundle\Interfaces\Service\Email\EmailSenderInterface;
use AppBundle\ValueObject\Email\Email;
use AppBundle\ValueObject\Email\EmailConfig;
use PHPMailer\PHPMailer\PHPMailer;

class EmailSenderService implements EmailSenderInterface
{
    private $phpMailer;

    /**
     * EmailSender constructor.
     * @param PHPMailer $phpMailer
     */
    public function __construct($phpMailer)
    {
        $this->phpMailer = $phpMailer;
    }

    public function sendEmail(EmailConfig $emailConfig, Email $email)
    {
        $this->phpMailer->isSMTP();
        $this->phpMailer->Host = $emailConfig->getServerName();
        $this->phpMailer->Username = $emailConfig->getUsername();
        $this->phpMailer->Password = $emailConfig->getPassword();
        $this->phpMailer->Port = $emailConfig->getPort();

        $this->phpMailer->setFrom($email->getSender());
        $this->phpMailer->addAddress($email->getRecipient());
        $this->phpMailer->Subject = $email->getSubject();
        $this->phpMailer->Body = $email->getBody();

        $this->phpMailer->send();
    }
}