<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 21:30
 */

namespace AppBundle\Controller;



use AppBundle\ValueObject\Email\Email;
use AppBundle\ValueObject\Email\EmailCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SiteController extends Controller
{
    public function indexAction(Request $request)
    {

        // Demonstration Area


        /**
         * Sending unique message with Google Provider.
         */
        $emailConfigProviderRepository = $this->get('email_provider_repository');
        $googleEmailProviderConfig = $emailConfigProviderRepository->getByName('google');

        $emailSenderService = $this->get('email_sender_service');
        $email = new Email("Testing", "test@gmail.com", "sky@is.limit", "I love PHP7 and u dude !!");

        $emailSenderService->sendEmail($googleEmailProviderConfig, $email);

        /**
         * Sending many messages load balanced.
         */
        $emailCollection = new EmailCollection();
        $emailCollection->append(new Email("Testing 1", "test@gmail.com", "sky@is.limit", "I love PHP7 and u dude !!"));
        $emailCollection->append(new Email("Testing 2", "test@gmail.com", "sky@is.limit", "I love PHP7 and u dude !!"));
        $emailCollection->append(new Email("Testing 3", "test@gmail.com", "sky@is.limit", "I love PHP7 and u dude !!"));

        $emailSenderBalancedService = $this->get('email_sender_balanced_service');
        $emailSenderBalancedService->sendEmailsToRandomProviders($emailCollection);


        return new Response("Please look at the code!!! No frontend here!");
    }
}