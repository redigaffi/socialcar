<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 22:17
 */

namespace AppBundle\Exception;


class WrongEmailProviderException extends \Exception
{
    protected $message = 'Wrong email provider';
    protected $code = -1;
}