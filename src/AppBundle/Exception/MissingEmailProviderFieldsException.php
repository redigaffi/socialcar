<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 23:48
 */

namespace AppBundle\Exception;


class MissingEmailProviderFieldsException extends \Exception
{
    protected $message = 'Retrieved email provider data is wrong, there are missing fields.';
    protected $code = -2;
}