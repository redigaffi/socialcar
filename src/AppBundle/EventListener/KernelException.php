<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 23:29
 */

namespace AppBundle\EventListener;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class KernelException
{
    private $environment;

    /**
     * KernelException constructor.
     * @param $environment
     */
    public function __construct(string $environment)
    {
        $this->environment = $environment;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // Set new response in prod environment, in dev we want to maintain Symfony's debugger.

        if ('prod' === $this->environment) {
            $response = new Response();
            $exceptionMessage = $event->getException()->getMessage();
            $response->setContent('<h1>An error happened, Please contact I.T department </h1>');
            $event->setResponse($response);

            // Send email with exception
            // Send exception to slack channel
            // ...
        }
    }


}