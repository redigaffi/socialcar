<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 22:55
 */

namespace AppBundle\Interfaces\Repository\EmailProviderRepository;


use AppBundle\ValueObject\Email\EmailConfig;

interface EmailProviderRepositoryInterface
{
    public function getAll(): array;
    public function getByName(string $name): EmailConfig;
}