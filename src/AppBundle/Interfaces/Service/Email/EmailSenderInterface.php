<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 18/09/17
 * Time: 22:51
 */

namespace AppBundle\Interfaces\Service\Email;


use AppBundle\ValueObject\Email\Email;
use AppBundle\ValueObject\Email\EmailConfig;

interface EmailSenderInterface
{
    public function sendEmail(EmailConfig $emailConfig, Email $email);
}