<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 19/09/17
 * Time: 13:20
 */

namespace AppBundle\Service\Email;



use AppBundle\Repository\EmailProviderRepository\EmailProviderConfigFileRepository;
use AppBundle\ValueObject\Email\Email;
use AppBundle\ValueObject\Email\EmailCollection;
use AppBundle\ValueObject\Email\EmailConfig;
use PHPMailer\PHPMailer\PHPMailer;

class EmailSenderLoadBalancedServiceTest extends \PHPUnit_Framework_TestCase
{

    private $emailSenderMock;
    private $emailProviderRepositoryMock;

    public function setUp()
    {
        $this->emailSenderMock = $this->getMockBuilder(EmailSenderService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->emailProviderRepositoryMock = $this->getMockBuilder(EmailProviderConfigFileRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testWithOneProviderCorrectly()
    {
        $emailCollection = $this->getEmailCollectionMock();
        $amountOfEmails = count($emailCollection);

        $this->emailSenderMock
            ->method('sendEmail')
            ->with($this->isInstanceOf(EmailConfig::class), $this->isInstanceOf(Email::class));


        $this->emailSenderMock
            ->expects($this->atLeast($amountOfEmails))
            ->method('sendEmail');

        $this->emailProviderRepositoryMock
            ->expects($this->atLeast(1))
            ->method('getAll')
            ->willReturn($this->getEmailProviders());

        $emailSenderLoadBalancedService = new EmailSenderLoadBalancedService($this->emailSenderMock, $this->emailProviderRepositoryMock);
        $emailSenderLoadBalancedService->sendEmailsToRandomProviders($emailCollection);


    }

    private function getEmailProviders():array
    {

        $emailConfi1 = $this->getMockBuilder(EmailConfig::class)
            ->disableOriginalConstructor()
            ->getMock();

        $emailConfi2 = $this->getMockBuilder(EmailConfig::class)
            ->disableOriginalConstructor()
            ->getMock();

        $emailConfi3 = $this->getMockBuilder(EmailConfig::class)
            ->disableOriginalConstructor()
            ->getMock();

        return [
            $emailConfi1,
            $emailConfi2,
            $emailConfi3
        ];
    }

    private function getEmailCollectionMock()
    {
        $email1 = $this->getMockBuilder(Email::class)
            ->disableOriginalConstructor()
            ->getMock();

        $email1->method('getSubject')->willReturn('subject');
        $email1->method('getSender')->willReturn('sender');
        $email1->method('getRecipient')->willReturn('recipient');
        $email1->method('getAttachments')->willReturn('attachments');
        $email1->method('getBody')->willReturn('body');

        $emailCollectionMock = $this->getMockBuilder(EmailCollection::class)
            ->setMethods(null)
            ->setConstructorArgs([[$email1]])
            ->getMock();

        return $emailCollectionMock;

    }


}
