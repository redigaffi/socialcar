<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 19/09/17
 * Time: 12:43
 */

namespace AppBundle\Repository\EmailProviderRepository;

use AppBundle\Exception\MissingEmailProviderFieldsException;
use AppBundle\Repository\EmailProviderRepository\EmailProviderConfigFileRepository;
use AppBundle\ValueObject\Email\EmailConfig;

class EmailProviderConfigFileRepositoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider correctEmailDataProvider
     */
    public function testGetAllCorrectly(array $availableProviders, array $emailConfig)
    {
        $emailProviderConfigFileRepository = new EmailProviderConfigFileRepository($availableProviders, $emailConfig);
        $allEmailProvidersParsed = $emailProviderConfigFileRepository->getAll();

        /**
         * @var EmailConfig $emailProviderParsedConfig
         */
        foreach ($allEmailProvidersParsed as $emailProviderParsedConfig) {
            $this->assertInstanceOf(EmailConfig::class, $emailProviderParsedConfig);

            $expectedConfig = $emailConfig[$emailProviderParsedConfig->getProviderName()];
            $this->assertEquals($expectedConfig['server_name'], $emailProviderParsedConfig->getServerName());
            $this->assertEquals($expectedConfig['username'], $emailProviderParsedConfig->getUsername());
            $this->assertEquals($expectedConfig['password'], $emailProviderParsedConfig->getPassword());
            $this->assertEquals($expectedConfig['port'], $emailProviderParsedConfig->getPort());
        }
    }

    /**
     * @dataProvider wrongEmailDataProvider
     */
    public function testGetAllThrowExceptionIncorrectParams(array $availableProviders, array $emailConfig)
    {
        $this->setExpectedException(MissingEmailProviderFieldsException::class);
        $emailProviderConfigFileRepository = new EmailProviderConfigFileRepository($availableProviders, $emailConfig);
        $emailProviderConfigFileRepository->getAll();
    }

    /**
     * @dataProvider correctEmailFindByDataProvider
     */
    public function testGetEmailProviderConfigByNameCorrectly(string $providerToFind, array $availableProviders, array $emailConfig)
    {

        $emailProviderConfigFileRepository = new EmailProviderConfigFileRepository($availableProviders, $emailConfig);
        $emailConfigFound = $emailProviderConfigFileRepository->getByName($providerToFind);

        $expectedEmailProviderFound = $emailConfig[$providerToFind];

        $this->assertEquals($expectedEmailProviderFound['server_name'], $emailConfigFound->getServerName());
        $this->assertEquals($expectedEmailProviderFound['username'], $emailConfigFound->getUsername());
    }

    public function correctEmailFindByDataProvider(): array
    {
        return [
            ['google', ['google', 'yahoo'],  ['google' => ['server_name' => 'google', 'username'=>'test', 'password' => 'test', 'port'=>500], 'yahoo' => ['server_name' => 'yahoo', 'username'=>'test1', 'password' => 'test1', 'port'=>500]]]
        ];
    }

    public function wrongEmailDataProvider(): array
    {
        return [
            [['google', 'yahoo'],  ['google' => ['a' => 'google', 'a'=>'test', 'a' => 'a', 'a'=>500], 'yahoo' => ['server_name' => 'yahoo', 'username'=>'test', 'password' => 'test', 'port'=>500]] ]
        ];
    }

    public function correctEmailDataProvider(): array
    {
        return [
            [['google', 'yahoo'],  ['google' => ['server_name' => 'google', 'username'=>'test', 'password' => 'test', 'port'=>500], 'yahoo' => ['server_name' => 'yahoo', 'username'=>'test1', 'password' => 'test1', 'port'=>500]] ]
        ];
    }
}
